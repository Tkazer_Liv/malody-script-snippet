-- @作者: Liv
-- @功能: 获取当前游玩谱面等级

function Init()
    -- 组件初始化
    -- {ver} 文本组件
    Imp_MapVersion = Module:Find("MapVersion")
end

function Update()
    -- 获取谱面Level
    local Level = GetMapLevel(Imp_MapVersion.Text)

end

-- 获取谱面Level函数
-- 若没有等级则返回0
function GetMapLevel(MapVersionText)
    local StartIndex, _ = string.find(MapVersionText, "Lv")

    if (StartIndex == nil) then
        StartIndex, _ = string.find(MapVersionText, "LV")
    end
    if (StartIndex == nil) then
        StartIndex, _ = string.find(MapVersionText, "lv")
    end
    if (StartIndex == nil) then
        StartIndex, _ = string.find(MapVersionText, "lV")
    end
    if (StartIndex == nil) then
        return 0
    end

    StartIndex = StartIndex + 2

    local TempString = ""
    local LevelCharCount = 0
    local TextLen = string.len(MapVersionText) - StartIndex

    for var = StartIndex, StartIndex + TextLen do
        -- 只需要两位数Level字串
        if (LevelCharCount >= 2) then
            break
        end
        -- 获取char并判断是否数字
        local Char = string.byte(MapVersionText, var)
        if (Char >= 48 and Char <= 57) then
            LevelCharCount = LevelCharCount + 1
            TempString = TempString..string.char(Char)
        end
    end

    if (LevelCharCount == 0) then
        return 0
    end

    local Level = tonumber(TempString)

    return Level
end