-- @作者: Liv
-- @功能: 获取各轨道Kps数据

function Init()
-- 全局输入时间戳队列
	g_InputQueue = {}
-- 全局轨道文本列表
-- 显示Kps数据的文本组件列表
	TrackTextList = {}
	table.insert(TrackTextList, Module:Find("TrackText_1"))
	table.insert(TrackTextList, Module:Find("TrackText_2"))
	table.insert(TrackTextList, Module:Find("TrackText_3"))
	table.insert(TrackTextList, Module:Find("TrackText_4"))
-- 若多k，则继续往轨道表中添加文本组件
		--table.insert(TrackTextList, Module:Find("TrackText_5"))
		--table.insert(TrackTextList, Module:Find("TrackText_6"))
		--table.insert(TrackTextList, Module:Find("TrackText_7"))
		--table.insert(TrackTextList, Module:Find("TrackText_8"))
-- 初始化全局时间戳轨道队列
	for i=1, #TrackTextList do
		table.insert(g_InputQueue, {})
	end
end

-- 获取指定轨道当前KPS
function GetTrackCurrentKps(TrackID)
	local CurrentTime = Game:Time()

	for i=0, #g_InputQueue[TrackID] do
		local TimeDis = CurrentTime-g_InputQueue[TrackID][i]
		if (TimeDis > 1000) then
		-- 弹出超时时间戳
			table.remove(g_InputQueue[TrackID][i], i)
		end
	end

	return #g_InputQueue[TrackID]
end

function Update()
	local TrackKps = {}

	-- 获取所有轨道当前Kps
	for i=1, #TrackTextList do
		table.insert(TrackKps, GetTrackCurrentKps(i))
	end

	-- Do somthing

end

function OnInput()
	local Event = Game:InputEvent()
	local HitType = Event:Type()
	local HitTrack = Event:HitX()

	-- 判断是否Hit存在轨道 且 是否为Down事件
	if (HitTrack ~= -1 and HitType == 1) then
		table.insert(g_InputQueue[HitTrack], Game:Time())
	end
end