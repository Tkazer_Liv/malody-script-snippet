-- @Author: Liv
-- @Last edit time: 2023/11/26 11:16

function Init()
    -- 组件初始化
    -- 用于显示MM值的文本组件
	Imp_MM = Module:Find("MM_Value")
    -- {score} 组件组件
	Imp_Score = Module:Find("Score")
    -- 谱面等级
    Num_MapLevel = GetMapLevel(Game:ChartInfo("Version"))
    -- 初始重置数据
    ResetData()
end

function Update()
    -- 更新MM值数据
    -- 更新分数
    Num_Score = tonumber(Imp_Score.Text)
    -- 若分数为零且Note计数不为零时，判断为重新游玩，进行重置数据
    if (Num_Score == 0 and Num_TotalNotes ~= 0) then
        ResetData()
    end
    -- 计算MM值
	local MM_Value = CalcMM(Num_Score, Num_Accuracy, Num_MapLevel, Num_TotalNotes)
    -- 显示MM值
    Imp_MM.Text = string.format("MM:[%.1f]",MM_Value)

end

function OnHit()
    -- 获取Note判定数据
    local Hit = Game:HitEvent()
    local HitResult = Hit:JudgeResult()
    
    -- Note总数增加
    Num_TotalNotes = Num_TotalNotes + 1

    -- 统计Note数据
    if (HitResult == 1) then
        Num_Best = Num_Best + 1
    end
    if (HitResult == 2) then
        Num_Cool = Num_Cool + 1
    end
    if (HitResult == 3) then
        Num_Great = Num_Great + 1
    end
    if (HitResult == 4) then
        Num_Miss = Num_Miss + 1
    end

    -- 计算Acc
    Num_Accuracy = CalcAcc(Num_Best, Num_Cool, Num_Great, Num_Miss)
end

-- 重置数据 (当重新游玩时)
function ResetData()
      -- 数据统计
    Num_Best = 0
    Num_Cool = 0
    Num_Great = 0
    Num_Miss = 0
    Num_TotalNotes = 0
    -- Acc
    Num_Accuracy = 0.0
    -- 分数
    Num_Score = 0
end

-- 计算MM函数
function CalcMM(Score, Accuracy, MapLevel, Notes)
    local S = 0.0
    local A = 0.0
    local L = 1.0
    local MM = 0.0
    
    S = Score / (2400 * Notes)

    L = L * (1.05 ^ (MapLevel > 10 and 9 or (MapLevel - 1)))

    if (MapLevel >= 10) then
        L = L * (1.04 ^ ((MapLevel > 15) and 5 or (MapLevel - 10)))
    end
    if (MapLevel >= 15) then
        L = L * (1.03 ^ ((MapLevel > 20) and 5 or (MapLevel - 15)))
    end
    if (MapLevel >= 20) then
        L = L * (1.02 ^ (MapLevel - 20))
    end

    if (Accuracy > 99.6) then
        A = (Accuracy == 100) and (0.05) or ((Accuracy - 99.6) / 10)
    end

    MM = ((S + A) * L - 1) * 60

    if (MM ~= MM or MM < 0.0) then
        MM = 0.0
    end

    return MM
end

-- 计算Acc函数
function CalcAcc(Best, Cool, Great, Miss)
    local Notes = Best + Cool + Great + Miss
    local Accuracy = (Best + Cool * 0.75 + Great * 0.4) / Notes
    if (Notes == 0) then
        return 100.0
    end
    return Accuracy * 100
end

-- 获取谱面Level函数
function GetMapLevel(MapVersionText)
    -- 获取"Lv"文本下标
    local StartIndex, _ = string.find(MapVersionText, "Lv")

    if (StartIndex == nil) then
        StartIndex, _ = string.find(MapVersionText, "LV")
    end
    if (StartIndex == nil) then
        StartIndex, _ = string.find(MapVersionText, "lv")
    end
    if (StartIndex == nil) then
        StartIndex, _ = string.find(MapVersionText, "lV")
    end
    if (StartIndex == nil) then
        return 0
    end

    StartIndex = StartIndex + 2

    local TempString = ""
    local LevelCharCount = 0
    local TextLen = string.len(MapVersionText) - StartIndex

    for var = StartIndex, StartIndex + TextLen do
        -- 只需要两位数Level字串
        if (LevelCharCount >= 2) then
            break
        end
        -- 获取char并判断是否数字
        local Char = string.byte(MapVersionText, var)
        if (Char >= 48 and Char <= 57) then
            LevelCharCount = LevelCharCount + 1
            TempString = TempString..string.char(Char)
        end
    end

    if (LevelCharCount == 0) then
        return 0
    end

    local Level = tonumber(TempString)

    return Level
end